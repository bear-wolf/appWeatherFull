const path = require('path')
// const HTMLWebpackPlugin = require('html-webpack-plugin')
// const {CleanWebpackPlugin} = require('clean-webpack-plugin')

const config = {
    context: path.resolve(__dirname, 'src'),
    mode: 'development', // "production" | "development" | "none"
    entry: {
        main:'./main.js',
        analytics: './analytics.js'
    },
    resolve: {
        extensions: ['*', '.mjs', '.js', '.json']
    },
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        // new HTMLWebpackPlugin({
        //     template: './index.html'
        // }),
        // new CleanWebpackPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.mjs$/,
                include: /node_modules/,
                type: 'javascript/auto'
            },
            {
                exclude: "/node_modules/",
                test: '/\.css$/',
                use: ['style-loader','css-loader'],
            },
            {
                exclude: "/node_modules/",
                test: '/\.png$/',
                use: ['file-loader'],
            },
        ]
    }
}

module.exports = config