import {Outlet} from "react-router-dom";
import React from "react";
import DashboardLayout from "./dashboard-layout";

const DashboardIndex = (props) => {
    return (<DashboardLayout {...props}>
                <Outlet/>
            </DashboardLayout>)
}

export default DashboardIndex