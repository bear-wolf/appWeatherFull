import React from "react";
import Layout from "./index";
import {Outlet, useRouteError} from "react-router-dom";
import MainPage from "./components/main";
import WeatherList from "./components/list";

const ErrorBoundaryLayout = () => (
    <ErrorBoundary>
        <Outlet/>
    </ErrorBoundary>
);

function ErrorBoundary() {
    let error = useRouteError();
    console.error(error);
    // Uncaught ReferenceError: path is not defined
    return <div>Dang!</div>;
}

export const DashboardRoutesModule = [
    {
        path: '/',
        element: <Layout></Layout>,
        elementError: <ErrorBoundaryLayout/>,
        children: [
            {
                path: '',
                element: <MainPage></MainPage>
            },
            {
                path: 'main',
                element: <MainPage></MainPage>
            },
            {
                path: 'list',
                element: <WeatherList></WeatherList>
            }
        ],
    }
]

export default DashboardRoutesModule
