import React, {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import request from "axios";
import 'dotenv/config'
import {setMessage} from "../../../app/reducers/message-slice";
import NotFound from "./not-found";
import * as moment from "moment/moment";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const API_HOST = process.env.REACT_APP_API_HOST
const SortingStatus = {
    createdAt: 'asc',
    name: 'asc'
}
const WeatherList = (props) => {
    const [isLoaded, setLoaded] = useState(false)
    const [entityList, setEntityList] = useState([])
    const dispatch = useDispatch()

    const setError = (message) => {
        return dispatch(setMessage({
            value: message || 'Something went wrong',
            timeout: 3000
        }))
    }

    const loadData = async (planID: any) => {
        const response: any = await request.get(`${API_HOST}/weather/list`)
        if (response.status !== 200) return setError('Something went wrong')

        setEntityList(response.data)
    }

    const sorting = (field: any, status: string) => {
        const list = entityList.sort((a, b) => {
            switch (status) {
                case 'asc':
                    SortingStatus[field] = 'desc';
                    return a[field] < b[field] ? 1 : -1
                case 'desc':
                    SortingStatus[field] = 'asc';
                    return a[field] > b[field] ? 1 : -1
            }
        });
        setEntityList([...list])
    }

    useEffect(() => {
        !isLoaded && loadData()
    }, []);

    return (<div className='weather-list mt-16'>
        <h1 className='title fs-4 mb-16'>Історія погодних даних</h1>

        {entityList.length > 0 && <div className="list">
            {/*<FontAwesomeIcon icon='sort-alt' />*/}
            <table className='table-block w-100'>
                <thead>
                <tr>
                    <th>#</th>
                    <th className='cursor-pointer' onClick={() => sorting('name', SortingStatus.name)}>
                        Місто
                    </th>
                    <th>Координата Lat</th>
                    <th>Координата Lon</th>
                    <th className='cursor-pointer' onClick={() => sorting('createdAt', SortingStatus.createdAt)}>
                        Дата створення
                    </th>
                </tr>
                </thead>
                <tbody>
                {entityList.map((item, index) => {
                    return <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.lat}</td>
                        <td>{item.lon}</td>
                        <td>{moment(item.createdAt).format('DD/MM/YYYY HH:mm')}</td>
                    </tr>
                })}
                </tbody>

            </table>
        </div>}

        {entityList.length === 0 && <NotFound message='Історія відсутня'></NotFound>}
    </div>)
}

export default WeatherList