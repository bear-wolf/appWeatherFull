import React from "react";
import "../style.scss";

const NotFound = ({message}) => {
    return (
        <div className='not-found'>
            {message}
        </div>
    )
}

export default NotFound