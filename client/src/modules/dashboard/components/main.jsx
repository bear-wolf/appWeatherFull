import React, {useState} from "react";
import Form from "react-bootstrap/Form";
import {Formik} from "formik";
import {hideMessage, setMessage} from "../../../app/reducers/message-slice";
import request from "axios";
import {useDispatch} from "react-redux";
import NotFound from "./not-found";
import Message from "./message";

const API_HOST = process.env.REACT_APP_API_HOST

const MainPage = (props) => {
    const [formValues, setFormValues] = useState({
        city: ''
    })
    const [data, setData] = useState(null)
    const [componentMessage, setComponentMessage] = useState({})
    const dispatch = useDispatch()

    const setError = (message) => {
        return dispatch(setMessage({
            value: message || 'Something went wrong',
            timeout: 3000
        }))
    }

    const handleSubmit = async (data) => {
        let response;

        try {
            response = await request.get(`${API_HOST}/weather/search?city=${data.city}`)
        } catch (error) {
            return setError(error.message)
        }

        if (response.status === 400) return setError('Something went wrong');
        if (!response.data) {
            return setComponentMessage({
                value: 'Дані відсутні',
                timeout: 5000
            })
        }
        setData(response.data)
    }

    return (<div className='main-page'>
        <h1 className='title fs-4 mb-16'>Форма запиту</h1>

        <div className="container-sm mb-16">
            <Formik
                initialValues={formValues}
                enableReinitialize={true}
                validate={values => {
                    const errors = {};
                    if (!values.city) errors.city = 'Обовязково';

                    return errors;
                }}
                onSubmit={handleSubmit}>
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      isSubmitting,
                      /* and other goodies */
                  }) => (
                    <form onSubmit={handleSubmit}>
                        <div className="row mb-2">
                            <div className="col-12">
                                <div className="form-group">
                                    <Form.Label htmlFor="city">Місто</Form.Label>
                                    <Form.Control
                                        id="city"
                                        name="city"
                                        className={`${errors.city && touched.city ? 'is-invalid' : ''}`}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.city}></Form.Control>
                                </div>
                            </div>
                        </div>

                        <button type="submit" className="btn btn-primary" disabled={isSubmitting}>
                            Відправити
                        </button>
                    </form>
                )}
            </Formik>
        </div>

        {data &&
            <div className='weather-information'>
                <table className="table">
                    <tr>
                        <td>Місто</td>
                        <td>{data.name}</td>
                    </tr>
                    <tr>
                        <td>Координати</td>
                        <td>{JSON.stringify(data.coord)}</td>
                    </tr>
                    <tr>
                        <td>Системні дані</td>
                        <td>{JSON.stringify(data.sys)}</td>
                    </tr>
                    <tr>
                        <td>Вітер</td>
                        <td>{JSON.stringify(data.wind)}</td>
                    </tr>
                </table>
            </div>}

        {(componentMessage || []).value && <div className='container-sm'>
            <Message callback={() => setComponentMessage({})} {...componentMessage}></Message>
        </div>}
    </div>)
}

export default MainPage