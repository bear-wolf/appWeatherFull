import iconAlert from '../../../assets/images/svgs/alert.svg';
import {useEffect, useState} from "react";

export interface iMessage {
    value: string;
    type: string; // alert | warning | info | success
    timeout?: number;
    callback: () => {}; // For manage the state outside
}

const Message = (props: iMessage) => {
    const [message: iMessage, setMessage] = useState({
        value: props.value || props.children || '',
        type: props.type || 'info',
        timeout: props.timeout || 0
    });

    useEffect(() => {
        message.timeout && setTimeout(() => props.callback(null), message.timeout)
    }, [message.value]);

    return (
         <div className={`message-container type-${message.type} mb-3`}>
             <div className={`message-container__message d-flex align-items-center`}>
                 <img src={iconAlert} className='mr-2' alt=''/>
                 &nbsp;{message.value}
             </div>
         </div>
    )
}
export default Message