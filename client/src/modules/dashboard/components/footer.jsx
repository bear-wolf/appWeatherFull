const Footer = () => {
    return (
        <footer>
            <div className="container-md">
                <address>
                    2024 ©TestForBeverlyHillCompany
                </address>
            </div>
        </footer>
    )
}

export default Footer