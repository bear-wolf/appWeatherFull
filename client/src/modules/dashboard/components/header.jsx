import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header className='mb-24'>
            <div className='container-md'>
                <nav className='d-flex'>
                    <NavLink className='logotype mr-24' to="/">TestForBeverlyHillCompany</NavLink>
                    <ul>
                        <li>
                            <NavLink to="/main">Запит</NavLink>
                        </li>
                        <li>
                            <NavLink to="/list">Історія</NavLink>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    )
}

export default Header