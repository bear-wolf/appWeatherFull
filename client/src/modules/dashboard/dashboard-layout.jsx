import React from "react";
import 'dotenv/config'
import {useDispatch, useSelector} from "react-redux";
import type {RootState} from "../../app/store";
import Message from "./components/message";
import {hideMessage} from "../../app/reducers/message-slice";
import Header from "./components/header";
import Footer from "./components/footer";

const DashboardLayout = (props) => {
    const message = useSelector((state: RootState) => state.message)
    const dispatch = useDispatch()

    return (
        <div className='layout'>
            <Header {...props}></Header>
            <div className="container-md h-100">
                {message.value && <Message callback={() => dispatch(hideMessage())} {...message}></Message>}
                {props.children}
            </div>
            <Footer></Footer>
        </div>);
};

export default DashboardLayout;