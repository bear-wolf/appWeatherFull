import React from 'react';
import ReactDOM from 'react-dom/client';
import './assets/styles/styles.scss';
import routers from "./router";
import store from './app/store'
import {Provider} from 'react-redux'
import {RouterProvider} from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <RouterProvider router={routers}/>
        </Provider>
    </React.StrictMode>
);
