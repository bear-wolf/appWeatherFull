import 'dotenv/config';
import {createBrowserRouter} from 'react-router-dom';
import DashboardRoutesModule from "./modules/dashboard/routers";

export default createBrowserRouter([
    ...DashboardRoutesModule
]);