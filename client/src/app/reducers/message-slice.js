import type {PayloadAction} from '@reduxjs/toolkit'
import {createSlice} from '@reduxjs/toolkit'

export interface MessageState {
    value: string;
    timeout: number;
    type: string;
}

const initialState: MessageState = {
    value: null,
    timeout: 0,
    type: 'info'
}
export const messageSlice = createSlice({
    name: 'message',
    initialState,
    reducers: {
        hideMessage: (state) => void(state.value = null),
        setMessage: (state, action: PayloadAction<MessageState>) => {
            state.value = action.payload.value
            state.timeout = action.payload.timeout || 0
            state.type = action.payload.type || initialState.type
        }
    }
})

// Action creators are generated for each case reducer function
export const {
    setMessage,
    hideMessage
} = messageSlice.actions

export default messageSlice.reducer