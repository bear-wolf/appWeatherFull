import type {PayloadAction} from '@reduxjs/toolkit'
import {createSlice} from '@reduxjs/toolkit'

export interface wheatherState {

}

const initialState: wheatherState = {

}
const localStoreKey = 'weather';

export const weatherSlice = createSlice({
    name: 'authorization',
    initialState,
    reducers: {
        setData: (state, action: PayloadAction<{}>) => {
            state.user = action.payload;
        }
    }
})

// Action creators are generated for each case reducer function
export const {
    setData,
} = weatherSlice.actions

export default weatherSlice.reducer