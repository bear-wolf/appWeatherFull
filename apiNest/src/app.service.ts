import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  isTest(): string {
    return 'Test OK';
  }

  getHello(): string {
    return 'Hello World !!!!';
  }
}
