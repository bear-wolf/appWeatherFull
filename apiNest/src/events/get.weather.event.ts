import {Injectable} from "@nestjs/common";

@Injectable()
export class GetWeatherEvent {
    constructor (public weather: any, public coordinate: any) {  }
}