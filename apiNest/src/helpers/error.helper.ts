import {HttpException, HttpStatus} from '@nestjs/common';


export const parseSQLError = (error: any) => {
    // Logger.error('ERROR', {
    //     date: new Date().toISOString(),
    //     ...error
    // })
    switch (error.name) {
        case 'ConnectionError': {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: error.meta.meta.name,
                message: 'Please check the connection to ElasticSearch',
            }, HttpStatus.BAD_REQUEST, {
                cause: error
            });
        }
    }
    return error
}

export default {
    parseSQLError
}