import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {LoggerMiddleware} from './middlewares/logger.middleware';
import {DashboardModule} from "./modules/dashboard/dashboard.module";
import {SharedModule} from "./modules/shared/shared.module";

@Module({
    controllers: [
        AppController
    ],
    imports: [
        SharedModule,
        DashboardModule
    ],
    providers: [
        LoggerMiddleware,
        AppService
    ],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(LoggerMiddleware)
            .forRoutes('sign-in');
    }
}
