import {Inject, Injectable} from '@nestjs/common';
import {WeatherEntity} from "../../shared/entities/weather.entity";
import {GeoEntity} from "../../shared/entities/geo.entity";
import {CACHE_MANAGER, CacheStore} from '@nestjs/cache-manager';
import {EventEmitter2} from "@nestjs/event-emitter";
import {OpenWeatherService} from "../../integrations/open-weather-map/open-weather.service";
import {GetWeatherEvent} from "../../../events/get.weather.event";
import {Weather} from "./weather.interface";
import {Coordinate} from "./coordinate.interface";

interface OpenWeatherAPIEvent {
    weather: Weather;
    coordinate: Coordinate;
}

@Injectable()
export class WeatherService {
    constructor(
        @Inject('WEATHER_ENTITY') private weatherRepository: typeof WeatherEntity,
        @Inject('GEO_ENTITY') private geoRepository: typeof GeoEntity,
        public readonly openWeatherService: OpenWeatherService,
        public eventEmitter: EventEmitter2,
        @Inject(CACHE_MANAGER) public cacheManager: CacheStore
    ) {
        this.eventEmitter.on('uploaded.open-weather-api', this.updateLocalDB.bind(this))
    }

    async updateLocalDB(data: OpenWeatherAPIEvent) {
        let object = await this.createGeoCoordinate(data.coordinate);
        await this.createWeather(object, data.weather);
    }

    async createGeoCoordinate(body: any): Promise<any> {
        // const record = await this.geoRepository.findOne({
        //     where: {
        //         name: body.name
        //     }
        // })
        //
        // if (record) return;

        let object = {
            name: body.name,
            ...(body.lat && {lat: body.lat}),
            ...(body.lon && {lon: body.lon}),
            createdAt: new Date()
        }
        return await this.geoRepository.create(object)
    }

    async createWeather(geo: any, body: any): Promise<any> {
        // const record = await this.weatherRepository.findOne({
        //     where: {
        //         name: body.name
        //     }
        // })
        //
        // if (record) return;

        let object = {
            geoID: geo.id,
            ...(body.weather && {weather: body.weather}),
            ...(body.main && {main: body.main}),
            ...(body.wind && {wind: body.wind}),
            ...(body.clouds && {clouds: body.clouds}),
            ...(body.sys && {sys: body.sys}),
            ...(body.timezone && {timezone: body.timezone}),
            ...(body.visibility && {visibility: body.visibility}),
            createdAt: new Date()
        }
        return await this.weatherRepository.create(object)
    }

    async getList(query?: any): Promise<any> {
        const {limit, createdAt, updatedAt} = query;

        let order: any = [
            ['createdAt', 'DESC']
        ]

        return await this.geoRepository.findAll({
            where: {},
            ...({limit: limit || 100}),
            order
        })
    }

    async search(query: any): Promise<any> {
        const isCash = await this.cacheManager.get(query.city);
        if (isCash) return isCash;

        const {weather, coordinate} = await this.openWeatherService.getWeather({
            city: query.city
        })
        this.eventEmitter.emit('uploaded.open-weather-api', new GetWeatherEvent(weather, coordinate))
        this.cacheManager.set(query.city, weather, 60 * 60);

        return weather;
    }

}
