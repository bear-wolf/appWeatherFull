export interface Weather {
    id?: number;
    geoID?: number;
    weather?: any;
    main?: any;
    wind?: any;
    clouds?: any;
    sys?: any;
    timezone?: any;
    visibility?: any;
    base?: any;
    name?: any;
    cod?: any;
    createdAt?: Date
    updatedAt?: Date
}