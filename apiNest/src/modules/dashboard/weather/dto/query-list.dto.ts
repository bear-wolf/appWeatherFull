import {IsNumber, IsOptional, IsString} from 'class-validator';

export class QueryListDto {
    @IsOptional()
    @IsNumber()
    limit: number

    @IsOptional()
    @IsString()
    createdAt: 'desc' | 'asc'

    @IsOptional()
    @IsString()
    updatedAt: 'desc' | 'asc'
}