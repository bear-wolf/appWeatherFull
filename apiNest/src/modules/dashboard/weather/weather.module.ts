import {Module} from '@nestjs/common';
import {WeatherController} from "./weather.controller";
import {WeatherService} from "./weather.service";
import {SharedModule} from "../../shared/shared.module";
import {OpenWeatherMapModule} from "../../integrations/open-weather-map/open-weather-map.module";

@Module({
    imports: [
        SharedModule,
        OpenWeatherMapModule
    ],
    controllers: [
        WeatherController
    ],
    exports: [
        WeatherService
    ],
    providers: [WeatherService]
})
export class WeatherModule {
}
