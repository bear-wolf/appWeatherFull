export interface Coordinate {
    id?: number;
    name?: string;
    local_names?: any;
    lat?: number;
    lon?: number;
    country?: string;
    state?: string;
}