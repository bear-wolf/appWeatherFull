import {Controller, Get, Query, Req, Res} from '@nestjs/common';
import {WeatherService} from "./weather.service";
import {Request, Response} from "express";
import {QueryDto} from "../../shared/dto/query.dto";
import {QueryListDto} from "./dto/query-list.dto";

@Controller('weather')
export class WeatherController {
    constructor(private readonly weatherRequestService: WeatherService) {
    }

    @Get('list')
    async get(@Query() query: QueryListDto, @Res() res: Response): Promise<any> {
        const list = await this.weatherRequestService.getList(query);

        return res.json(list);
    }

    @Get('/search')
    async search(@Query() query: QueryDto, @Res() res: Response): Promise<any> {
        const list = await this.weatherRequestService.search(query);

        return res.json(list);
    }
}