import {Module} from '@nestjs/common';
import {WeatherModule} from "./weather/weather.module";
import {OpenWeatherMapModule} from "../integrations/open-weather-map/open-weather-map.module";

@Module({
    imports: [
        OpenWeatherMapModule,
        WeatherModule
    ],
    controllers: [],
    exports: [],
    providers: []
})
export class DashboardModule {
}
