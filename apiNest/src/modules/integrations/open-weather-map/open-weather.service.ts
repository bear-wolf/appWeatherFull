import {Injectable, Logger} from '@nestjs/common';
import {HttpService} from "@nestjs/axios";
import {catchError, firstValueFrom} from "rxjs";
import 'dotenv/config';
import * as process from "node:process";
import {OpenWeatherRequest} from "./open-weather-request.interface";
import {AxiosError} from "axios";

@Injectable()
export class OpenWeatherService {
    private readonly logger = new Logger('openweathermap');
    private routes = {
        geo: `http://api.openweathermap.org/geo/1.0/direct?q={city}&limit={limit}&appid=${process.env.WEATHER_API_KEY}`,
        weather: `https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&exclude={exclude}&appid=${process.env.WEATHER_API_KEY}`
    }
    constructor(private readonly httpService: HttpService) {}

    catchError(error: AxiosError) {
        this.logger.error(error.response.data);
        throw 'An error happened!';
    }

    async getWeather(options: OpenWeatherRequest): Promise<{coordinate: any, weather: any}> {
        const coordinate: any[] = await this.getGeoByCity(options);
        const coord: any = coordinate.length ? coordinate[0] : {};
        let weatherRoute = this.routes.weather
                .replace('{lat}', coord.lat)
                .replace('{lon}', coord.lon)
                .replace('{exclude}', 'hourly,daily');
        const { data: weather } = await firstValueFrom(
            this.httpService.get<any[]>(weatherRoute)
                .pipe(catchError(this.catchError.bind(this)))
        );

        return {
            coordinate: coord,
            weather
        };
    }

    async getGeoByCity(options: OpenWeatherRequest): Promise<any> {
        let geo = this.routes.geo
            .replace('{city}', options.city)
            .replace('{limit}', (options.limit || 5).toString());

        const { data } = await firstValueFrom(
            this.httpService.get<any[]>(geo)
                .pipe(catchError(this.catchError.bind(this)))
        );
        return data;
    }

}
