import {Module} from '@nestjs/common';
import {OpenWeatherService} from "./open-weather.service";
import {HttpModule} from "@nestjs/axios";

@Module({
    imports: [
        HttpModule
    ],
    controllers: [],
    exports: [
        OpenWeatherService
    ],
    providers: [
        OpenWeatherService
    ]
})
export class OpenWeatherMapModule {
}
