export interface OpenWeatherRequest {
    limit?: number;
    city: string;
}