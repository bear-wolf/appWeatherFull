import { Column, Table } from 'sequelize-typescript';
import {Model} from "sequelize-typescript";

@Table({
  tableName: 'Geos',
})
export class GeoEntity extends Model {
  @Column({
    autoIncrement: true,
    primaryKey: true
  })
  id: number;

  @Column({})
  name: string;

  @Column({})
  lat: string;

  @Column({})
  lon: string;

  @Column({
    field: 'created_at'
  })
  createdAt: Date;

  @Column({
    defaultValue: null,
    field: 'updated_at'
  })
  updatedAt: Date;
}