import {WeatherEntity} from './weather.entity';
import {GeoEntity} from "./geo.entity";

export const EntityProviders = [
    {
        provide: 'WEATHER_ENTITY',
        useValue: WeatherEntity
    },
    {
        provide: 'GEO_ENTITY',
        useValue: GeoEntity
    }
]

export const getEntities = EntityProviders.map(item => item.useValue)