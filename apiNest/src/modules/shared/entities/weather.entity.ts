import {BelongsTo, Column, DataType, ForeignKey, Table} from 'sequelize-typescript';
import {GeoEntity} from "./geo.entity";
import {Model} from "sequelize-typescript";

@Table({
    tableName: 'Weathers',
})
export class WeatherEntity extends Model {
    @Column({
        autoIncrement: true,
        primaryKey: true
    })
    id: number;

    // @ts-ignore
    @ForeignKey(() => GeoEntity)
    @Column({
        references: 'Geos',
        field: 'geo_id'
    })
    geoID: string;

    // @ts-ignore
    @BelongsTo(() => GeoEntity)
    geo: GeoEntity;

    @Column({
        type: DataType.JSONB
    })
    weather: any;

    @Column({
        type: DataType.JSONB
    })
    main: any;

    @Column({
        type: DataType.JSONB
    })
    wind: any;

    @Column({
        type: DataType.JSONB
    })
    clouds: any;

    @Column({
        type: DataType.JSONB
    })
    sys: any;

    @Column({
        type: DataType.INTEGER
    })
    timezone: any;

    @Column({
        type: DataType.INTEGER
    })
    visibility: any;

    @Column({
        type: DataType.STRING
    })
    base: string;

    @Column({
        type: DataType.STRING
    })
    name: string;

    @Column({
        type: DataType.INTEGER
    })
    cod: number;

    @Column({
        type: DataType.DATE,
        defaultValue: new Date(),
        field: 'created_at'
    })
    createdAt: Date;

    @Column({
        type: DataType.DATE,
        defaultValue: null,
        field: 'updated_at'
    })
    updatedAt: Date;
}