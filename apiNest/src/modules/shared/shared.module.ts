import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {SequelizeModule} from '@nestjs/sequelize';
import 'dotenv/config';
import {CacheModule} from '@nestjs/cache-manager';
import {EntityProviders, getEntities} from './entities/entity.providers';
import {utilities as nestWinstonModuleUtilities, WinstonModule} from 'nest-winston';
import * as winston from 'winston';
import {EventEmitterModule} from '@nestjs/event-emitter';
import {LoggerMiddleware} from '../../middlewares/logger.middleware';

const {
    PGSQL_DB_HOST, PGSQL_DB_PORT, PGSQL_DB_NAME, PGSQL_DB_USERNAME, PGSQL_DB_PASSWORD,
} = process.env;

@Module({
    imports: [
        CacheModule.register(),
        EventEmitterModule.forRoot(),
        WinstonModule.forRoot({
            transports: [
                new winston.transports.Console({
                    format: winston.format.combine(
                        winston.format.timestamp(),
                        winston.format.ms(),
                        nestWinstonModuleUtilities.format.nestLike('MyApp', {
                            colors: true,
                            prettyPrint: true,
                            processId: true,
                        }),
                    ),
                }),
                new winston.transports.File({
                    filename: 'logs/warn.log',
                    level: 'warn',
                }),
                new winston.transports.File({
                    filename: 'logs/debug.log',
                    level: 'debug',
                }),
                new winston.transports.File({
                    filename: 'logs/error.log',
                    level: 'error',
                }),
                // other transports...
            ],
            // other options
        }),
        SequelizeModule.forRoot({
            dialect: 'postgres',
            host: PGSQL_DB_HOST,
            port: Number(PGSQL_DB_PORT),
            username: PGSQL_DB_USERNAME,
            password: PGSQL_DB_PASSWORD,
            database: PGSQL_DB_NAME,
            autoLoadModels: true,
            synchronize: false
        }),
        SequelizeModule.forFeature(getEntities), // connect to DB
    ],
    controllers: [],
    providers: EntityProviders,
    exports: [
        ...EntityProviders,
        WinstonModule,
        CacheModule
    ],
})
export class SharedModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(LoggerMiddleware)
            .forRoutes('v1');
    }
}
