import {IsString} from 'class-validator';

export class QueryDto {
    @IsString()
    city: string
}