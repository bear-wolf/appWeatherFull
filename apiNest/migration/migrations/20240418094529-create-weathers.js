const {DataTypes} = require("sequelize");
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Weathers', {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            geoID: {
                references: { model: 'Geos', key: 'id' },
                type: DataTypes.INTEGER,
                allowNull: false,
                field: 'geo_id'
            },
            weather: {
                type: DataTypes.JSONB
            },
            main: {
                type: DataTypes.JSONB
            },
            wind: {
                type: DataTypes.JSONB
            },
            clouds: {
                type: DataTypes.JSONB
            },
            sys: {
                type: DataTypes.JSONB
            },
            timezone: {
                type: DataTypes.INTEGER
            },
            visibility: {
                type: DataTypes.INTEGER
            },
            base: {
                type: DataTypes.STRING(50)
            },
            name: {
                type: DataTypes.STRING(100)
            },
            cod: {
                type: DataTypes.INTEGER
            },
            createdAt: {
                type: DataTypes.DATE,
                default: Date.now(),
                field: 'created_at'
            },
            updatedAt: {
                type: DataTypes.DATE,
                defaultValue: null,
                field: 'updated_at'
            }
        })
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Weathers');
    }
};