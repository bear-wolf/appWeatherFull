const {DataTypes} = require("sequelize");
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Geos', {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            name: {
                type: DataTypes.STRING(100),
                allowNull: false,
                index: true
            },
            lat: {
                type: DataTypes.STRING(25),
                allowNull: false,
                index: true
            },
            lon: {
                type: DataTypes.STRING(25),
                allowNull: false,
                index: true
            },
            createdAt: {
                type: DataTypes.DATE,
                default: Date.now(),
                field: 'created_at'
            },
            updatedAt: {
                type: DataTypes.DATE,
                defaultValue: null,
                field: 'updated_at'
            }
        })
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Geos');
    }
};